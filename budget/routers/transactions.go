package routers

import (
	"bitbucket.org/uscacm/acm-internal-tooling/budget/controllers"
	"github.com/gorilla/mux"
)

// connects path patterns to controller actions
func setBudgetRouters(router *mux.Router) *mux.Router {
	router.HandleFunc("/txn", controllers.getTransactions).Methods("GET")
	router.HandleFunc("/txn", controllers.createTransaction).Methods("POST")
	router.HandleFunc("/txn/{id}", controllers.getTransactionById).Methods("GET")
	router.HandleFunc("/txn/{id}", controllers.deleteTransactionById).Methods("DELETE")

	return router
}
