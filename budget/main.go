package main

import (
	"fmt"

	"bitbucket.org/uscacm/acm-internal-tooling/budget/routers"
)

func main() {
	router := routers.InitRoutes()

	fmt.Printf("%+v", router)
}
